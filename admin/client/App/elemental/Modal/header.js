import React, { PropTypes } from 'react';
import { css } from 'glamor';
import GlyphButton from '../GlyphButton';
import theme from '../../../theme';

const lookUpTable = {
	'Create a new Post': 'Adicionar novo Post',
	'Create a new Post Category': 'Adicionar nova Categoria',
	'Create a new User': 'Adicionar novo Utilizador',
	'Create a new Banner': 'Adicionar novo Banner',
	'Create a new Service': 'Adicionar novo Serviço',
	'Create a new Membro Administrativo': 'Adicionar novo Membro',
	'Create a new Residência Sénior': 'Adicionar nova Info RS',
	'Create a new Cuidados Continuado': 'Adicionar nova info CC',
	'Create a new Cuidados Continuados': 'Adicionar nova info CC',
	'Create a new Serviço': 'Adicionar novo Serviço',
	'Create a new Serviços Complementare': 'Adicionar novo Serviço Complementar',
	'Create a new Serviços Complementares': 'Adicionar novo Serviço Complementar',
	'Create a new Info Urgência': 'Adicionar nova Info',
	'Create a new Médico': 'Adicionar novo Médico',
	'Create a new Acordo': 'Adicionar novo Acordo',
	'Create a new Parceiro': 'Adicionar novo Parceiro',
	'Create a new Info Hospital': 'Adicionar nova Info',
	'Create a new Concelhos Próximo': 'Adicionar novo Concelho',
};

function ModalHeader ({
	children,
	className,
	showCloseButton,
	text,
	...props
}, {
	onClose,
}) {
	// Property Violation
	if (children && text) {
		console.error('Warning: ModalHeader cannot render `children` and `text`. You must provide one or the other.');
	}
	return (
		<div {...props} className={css(classes.header, className)}>
			<div className={css(classes.grow)}>
				{text ? (
					<h4 className={css(classes.text)}>
						{lookUpTable[text] !== undefined ? lookUpTable[text] : text}
					</h4>
				) : children}
			</div>
			{!!onClose && showCloseButton && (
				<GlyphButton
					aphroditeStyles={classes.close}
					color="cancel"
					glyph="x"
					onClick={onClose}
					variant="link"
				/>
			)}
		</div>
	);
};

ModalHeader.propTypes = {
	children: PropTypes.node,
	onClose: PropTypes.func,
	showCloseButton: PropTypes.bool,
	text: PropTypes.string,
};
ModalHeader.contextTypes = {
	onClose: PropTypes.func.isRequired,
};

const classes = {
	header: {
		alignItems: 'center',
		borderBottom: `2px solid ${theme.color.gray10}`,
		display: 'flex',
		paddingBottom: theme.modal.padding.header.vertical,
		paddingLeft: theme.modal.padding.header.horizontal,
		paddingRight: theme.modal.padding.header.horizontal,
		paddingTop: theme.modal.padding.header.vertical,
	},

	// fill space to push the close button right
	grow: {
		flexGrow: 1,
	},

	// title text
	text: {
		color: 'inherit',
		fontSize: 18,
		fontWeight: 500,
		lineHeight: 1,
		margin: 0,
	},
};

module.exports = ModalHeader;
